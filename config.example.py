import logging

# Основные настройки
BOT_TOKEN = "420:TOKEN"
ADMINS = [123, 456, 890]

# Настройки логирования (ERROR INFO DEBUG), обычно используют INFO
LOGGING_LEVEL = logging.DEBUG

# База данных
DB_URL = "sqlite:///mydb.sqlite"

ALL_PERMISSIONS = {
    "can_change_info",
    "can_post_messages",
    "can_edit_messages",
    "can_delete_messages",
    "can_invite_users",
    "can_restrict_members",
    "can_pin_messages",
    "can_promote_members"
}

# Antispam
LOAD_ANTISPAM_MODULE = False  # True если ты у тебя есть middlewares/antispam.py
WARNS_MAX = 2
MESSAGE_CACHE_SIZE = 4
SPAMMING_NOW = False


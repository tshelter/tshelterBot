from aiogram import types
from bot import dp


@dp.callback_query_handler(is_admin=True, text="cancel_admin:")
async def bot_cancel_4_admin(call: types.CallbackQuery):
    await call.message.delete()


@dp.callback_query_handler(text="cancel")
async def bot_cancel(call: types.CallbackQuery):
    await call.message.delete()

from aiogram import types
from bot import dp, db, utils
import config
import strings as s


@dp.message_handler(is_admin=True, commands=["unmake"])
async def bot_make(message: types.Message):
    if message.reply_to_message:
        to_unmake = message.reply_to_message.from_user.id

    else:
        args = message.get_args()
        if not args or not args.isdigit():
            return await message.reply(s.example.format(example=s.unmake_cmd))

        to_unmake = int(args)

    if to_unmake in config.ADMINS:
        return await message.reply(s.is_bot_admin)

    admin = db.Admin.get_or_none(user=to_unmake)

    if admin is None:
        return message.reply(s.not_admin)

    await utils.demote(to_unmake)
    await message.reply(s.demoted.format(user_id=to_unmake))

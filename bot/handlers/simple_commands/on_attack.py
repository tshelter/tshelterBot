from aiogram import types
from bot import dp
import config
import strings as s


@dp.message_handler(lambda m: m.from_user.id in config.ADMINS, commands=["on_attack"])
async def bot_fb(message: types.Message):
    if config.LOAD_ANTISPAM_MODULE:
        config.SPAMMING_NOW = not config.SPAMMING_NOW
        await message.reply(s.anti_spam_status.format(status=config.SPAMMING_NOW))

    else:
        await message.reply(s.anti_spam_not_loaded)


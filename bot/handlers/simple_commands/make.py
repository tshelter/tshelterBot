from aiogram import types
from bot import dp, db, utils
import config
import strings as s


@dp.message_handler(is_admin=True, commands=["make"])
async def bot_make(message: types.Message):
    if message.reply_to_message:
        to_make = message.reply_to_message.from_user.id

    else:
        args = message.get_args()
        if not args or not args.isdigit():
            return await message.reply(s.example.format(example=s.make_cmd))

        to_make = int(args)

    if to_make in config.ADMINS:
        return await message.reply(s.is_bot_admin)

    keyboard = types.InlineKeyboardMarkup()

    with db.db:
        for preset in db.Preset.select():
            keyboard.add(
                types.InlineKeyboardButton(preset.name, callback_data=f"make:{to_make}:{preset.id}")
            )

    await message.reply("Выбери роль", reply_markup=keyboard)


@dp.callback_query_handler(is_admin=True, text_startswith="make:")
async def bot_make_call(call: types.CallbackQuery):
    to_make, preset_id = utils.get_args_callback(call, exactly=2)

    if to_make in config.ADMINS:
        return await call.message.edit_text(s.is_bot_admin)

    with db.db:
        preset = db.Preset.get(id=preset_id)
        user, _ = db.User.get_or_create(id=to_make)
        admin, new = db.Admin.get_or_create(user=user.id, defaults={"preset": preset})

        if not new:
            admin.preset = preset
            admin.save()

    values = {key: getattr(preset, key) for key in config.ALL_PERMISSIONS.keys()}

    await utils.promote(to_make, admin.title, **values)
    await call.message.edit_text("<a href='tg://user?id={user_id}'>Чел</a> стал <b>{name}</b>".format(name=preset.name,
                                                                                                     user_id=to_make))

from aiogram import types
from bot import dp, db, utils
import strings as s


@dp.message_handler(commands=["nick"])
async def bot_make(message: types.Message):
    with db.db:
        admin = db.Admin.get_or_none(user=message.from_user.id)

    if not admin:
        return await message.reply(s.you_are_not_admin)

    new_title = message.get_args()

    if not new_title or len(new_title) > 16:
        return await message.reply(s.example.format(example=s.nick_cmd))

    await utils.set_title(admin.user.id, new_title)
    await message.reply(s.changed_nick.format(new_title=new_title))

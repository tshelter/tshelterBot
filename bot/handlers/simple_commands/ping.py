from aiogram import types
from bot import dp
import strings as s


@dp.message_handler(commands=["ping"])
async def bot_fb(message: types.Message):
    await message.reply(s.ping)


@dp.message_handler(is_admin=True, commands=["aping"])
async def bot_fb(message: types.Message):
    await message.reply(s.admin_ping)

from aiogram import types
from bot import dp, db
import strings as s


@dp.message_handler(is_admin=True, commands=["add"])
async def bot_presets(message: types.Message):
    args = message.get_args()

    if len(args) > 16 or len(args) == 0 or args.isdigit():
        return await message.reply(s.example.format(example=s.add_cmd))

    with db.db:
        preset = db.Preset.create(name=args)

    await message.reply(s.added_preset.format(name=preset.name))

import asyncio

from aiogram import types
from bot import dp, db, utils
import strings as s


@dp.callback_query_handler(is_admin=True, text_startswith="sure_delete_preset:")
async def bot_presets(call: types.CallbackQuery):
    preset_id, = utils.get_args_callback(call, exactly=1)
    preset_id = int(preset_id)

    await call.message.edit_text("Удаляю пресет, снося всем админки нахуй.")

    with db.db:
        counter = 0
        errors = []
        all_admins = db.Admin.select().where(db.Admin.preset.id == preset_id).count()
        for admin in db.Admin.select().join(db.Preset).join(db.User).where(db.Admin.preset.id == preset_id):
            try:
                await utils.demote(admin.user.id)
                counter += 1
            except Exception as e:
                errors.append(await call.message.reply(
                    "Админа {user_id} демотнуть не удалось, пушто {e}".format(user_id=admin.user.id, e=e)
                ))

        preset = db.Preset.get(preset_id)
        preset.delete()

        # TODO: replace to s.lol
        await call.message.edit_text("Preset: {name} удален\nИз админов: {all_admins}/{demoted_admins}: было понижено"
            .format(
            name=preset.name, all_admins=all_admins, demoted_admins=counter
        ))

        await asyncio.sleep(len(errors) * 2)
        for error in errors:
            await error.delete()

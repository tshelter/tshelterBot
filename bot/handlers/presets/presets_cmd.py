from aiogram import types
from bot import dp, db, utils
import config
import strings as s


def get_presets():
    keyboard = types.InlineKeyboardMarkup(row_width=2)

    counter = 0
    with db.db:
        for preset in db.Preset.select():
            keyboard.add(
                types.InlineKeyboardButton(preset.name, callback_data=f"preset:{preset.id}")
            )
            counter += 1

    return {
        "text": s.presets.format(count=counter),
        "reply_markup": keyboard
    }


@dp.message_handler(is_admin=True, commands=["presets"])
async def bot_presets(message: types.Message):
    await message.reply(**get_presets())


@dp.callback_query_handler(is_admin=True, text="presets")
async def bot_presets(call: types.CallbackQuery):
    await call.message.edit_text(**get_presets())


@dp.callback_query_handler(is_admin=True, text_startswith="preset:")
async def bot_presets(call: types.CallbackQuery):
    preset_id, = utils.get_args_callback(call, exactly=1)

    with db.db:
        preset = db.Preset.get(id=preset_id)

    keyboard = types.InlineKeyboardMarkup(row_width=1)

    for key, value in config.ALL_PERMISSIONS.items():
        text = (s.yes if getattr(preset, key) else s.no) + value
        keyboard.add(types.KeyboardButton(
            text, callback_data=f"edit_preset:{preset.id}:{key}"
        ))

    keyboard.add(
        types.InlineKeyboardButton(s.delete, callback_data=f"delete_preset:{preset.id}"),
        types.InlineKeyboardButton(s.go_back, callback_data="presets")
    )

    await call.message.edit_text(s.preset.format(name=preset.name), reply_markup=keyboard)


@dp.callback_query_handler(is_admin=True, text_startswith="edit_preset:")
async def bot_presets(call: types.CallbackQuery):
    preset_id, key = utils.get_args_callback(call, exactly=2)

    if key == "can_invite_users":
        await call.answer(s.must_have_one_permission)

    preset = db.Preset.get(id=preset_id)

    with db.db:
        setattr(preset, key, not getattr(preset, key))
        preset.save()

    keyboard = types.InlineKeyboardMarkup(row_width=2)

    for key, value in config.ALL_PERMISSIONS.items():
        text = ("+ " if getattr(preset, key) else "- ") + value
        keyboard.add(types.KeyboardButton(
            text, callback_data=f"edit_preset:{preset.id}:{key}"
        ))

    keyboard.add(types.InlineKeyboardButton(
        s.go_back, callback_data="presets"
    ))

    await call.message.edit_text("ok", reply_markup=keyboard)


@dp.callback_query_handler(is_admin=True, text_startswith="delete_preset:")
async def bot_presets(call: types.CallbackQuery):
    preset_id, = utils.get_args_callback(call, exactly=1)

    keyboard = types.InlineKeyboardMarkup(row_width=1)

    keyboard.add(
        types.InlineKeyboardButton(s.sure_delete, callback_data=f"sure_delete_preset:{preset_id}")
    )


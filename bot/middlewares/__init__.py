import config


def middlewares_init(dp):
    from . import registration
    dp.middleware.setup(registration.RegisteringMiddleware())

    if config.LOAD_ANTISPAM_MODULE:
        try:
            from . import antispam
            dp.middleware.setup(antispam.AntiSpamMiddleware())
        except ImportError:
            print("У вас нет модуля antispam!")

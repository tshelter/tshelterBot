from aiogram import executor
from bot import dp, setup_bot


if __name__ == '__main__':
    setup_bot()

    executor.start_polling(dp, skip_updates=True)

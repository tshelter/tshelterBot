from peewee import Model, IntegerField, BooleanField, CharField, ForeignKeyField
from playhouse.db_url import connect
import config

db = connect(config.DB_URL, autocommit=False)

# BooleanField(null=False, default=False) = BooleanField(null=False, default=False)


class BaseModel(Model):

    class Meta:
        database = db


class User(BaseModel):
    id = IntegerField(null=False, unique=True, primary_key=True)  # уникальный айди пользователя
    banned = BooleanField(null=False, default=False)  # заблокирован ли пользователь

    class Meta:
        db_table = "users"
        database = db


class Preset(BaseModel):
    name = CharField(null=False, unique=True)
    can_change_info = BooleanField(null=False, default=False)
    can_delete_messages = BooleanField(null=False, default=False)
    can_restrict_members = BooleanField(null=False, default=False)
    can_invite_users = BooleanField(null=False, default=True)

    can_pin_messages = BooleanField(null=False, default=False)
    #  can_manage_voice_chats = BooleanField(null=False, default=False)
    can_promote_members = BooleanField(null=False, default=False)


class Admin(BaseModel):
    user = ForeignKeyField(User, null=False)
    preset = ForeignKeyField(Preset, default=None, null=True)
    title = CharField(max_length=16, null=False, default="НеПоставилНик")  # если что 13 символов

    class Meta:
        db_table = "admins"


with db:
    db.create_tables([
        User, Preset, Admin
    ])

    if not Preset.get_or_none():
        Preset.create(name="Тест")

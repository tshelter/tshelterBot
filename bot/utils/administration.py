import config
from bot import bot


async def promote(user_id: int, title, chat: int = config.MAIN_CHAT_ID, **kwargs):
    await bot.promote_chat_member(chat, user_id, **kwargs)
    return await bot.set_chat_administrator_custom_title(chat, user_id, title)


async def set_title(user_id: int, title, chat: int = config.MAIN_CHAT_ID):
    return await bot.set_chat_administrator_custom_title(chat, user_id, title)


async def demote(user_id: int, chat: int = config.MAIN_CHAT_ID):
    return await bot.promote_chat_member(
        chat, user_id, is_anonymous=False,
        can_change_info=False,
        can_post_messages=False,
        can_edit_messages=False,
        can_delete_messages=False,
        can_invite_users=False,
        can_restrict_members=False,
        can_pin_messages=False, can_promote_members=False
    )

from logging import getLogger
from aiogram import types
from aiogram.dispatcher.handler import CancelHandler
import strings as s
from bot import dp

logger = getLogger(__name__)


@dp.errors_handler(exception=Exception)
async def catch_all(update: types.Update, error):
    if isinstance(error, CancelHandler):
        return True

    logger.error("Error!", exc_info=True)

    if update.message:
        await update.message.reply(s.error_occurred.format(error=error))

    elif update.callback_query:
        await update.callback_query.answer(s.error_occurred.format(error=error))

    return True

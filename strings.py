#  simple_commands/ping.py
ping = "понг"
admin_ping = "о четко, ты админ"

#  simple_commands/on_attack.py
anti_spam_status = "Суперантиспам: {status}"
anti_spam_not_loaded = "У вас нет antispam.py"

#  simple_commands/make.py and unmake.py
is_bot_admin = "Он и dungeon master этого бота!"
not_admin = "Он не админ, ты что удумал"
demoted = "ну <a href='tg://user?id={user_id}'>чел</a> не админ"

#  simple_commands/nick
you_are_not_admin = "Ты не админ!"
changed_nick = "Теперь твой ник <b>{new_title}</b>"

#  presets/add.py
added_preset = "Создал preset <b>{name}</b>"

#  presets/presets_cmd.py
presets = "Всего presets: {count}"
preset = "Preset: {name}"
must_have_one_permission = "Даже у отбитой фемки есть право добавлять!"

not_registered = "Вы не зарегистрированы в боте"

#  error_handlers/catch_all
error_occurred = "Взлом жопы:\n{error}"

#  buttons ok
yes = "+ "
no = "- "
go_back = "Назад"
delete = "Удалить"
sure_delete = "Вот точно нафиг удалить"

#  commands example
example = "Вот тебе сука пример: {example}"
make_cmd = "/make реплай|айди_чела"
unmake_cmd = "/unmake реплай|айди_чела"
add_cmd = "/add название\nБез чисел и не больше 16ти символов"
nick_cmd = "/nick ник\nНе более 16 символов"


# middlewares/antispam.py
do_not_spam = "DO NOT SPAM"
